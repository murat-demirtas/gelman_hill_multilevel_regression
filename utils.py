import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt

def sim(model_results, size=10):
    """
    Simulate samples from coefficients of a linear model (analogous to R function sim() from Gelman and Hill).
    The standard deviation of residuals $\hat{\sigma}$ is chi-squared while the model coefficients
    $\hat{beta}$ are normal.
    Both uncertainty in $\hat{\sigma}$ and $\hat{beta}$ is taken into account.
    PARAMETERS:
        model_results : the results object returned from `fit()` method of a statsmodels linear model class
        size : int the number of simulations to run
    RETURN:
        beta_sim : np.array of shape [size, len(model_results.params)]
    """
    beta_hat = model_results.params
    cov_beta = model_results.cov_params()
    df = model_results.df_resid
    sigma_hat = np.sqrt(np.sum(model_results.resid**2) / df)
    V_beta = cov_beta / sigma_hat**2
    X = np.random.chisquare(df, size=size)
    sigma_sim = sigma_hat * np.sqrt(df / X)
    beta_sim = [np.random.multivariate_normal(beta_hat, (sigma_sim[i]**2) * V_beta) for i in range(size)]
    return np.array(beta_sim)

def standardize(data, center_ref=None, scale_factor=None):
    """
    Standardize variable in `data`.
    PARAMETERS: 
        data : np.array or pandas DataFrame with samples on the rows and variable on the columns
        center_ref : 
    """
    if center_ref is None:
        center_ref = data.mean()
    if scale_factor is None:
        scale_factor = 2 * data.std()
    elif np.isscalar(scale_factor):
        scale_factor = scale_factor * data.std()        
    #assert (len(scale_factor)==np.shape(data)[1]), "len(scale_factor) has to be equal to the number of columns in data!"
    #assert (len(center_ref)==np.shape(data)[1]), "len(center_ref) has to be equal to the number of columns in data!"
    return (data - center_ref) / scale_factor

def plot_model_coefs(model, ax=None, include_const=False):
    if ax is None:
        fig, ax = plt.subplots()
    if not include_const:
        ax.errorbar(model.params.keys()[1:], model.params[1:], model.bse[1:]*2, fmt='none')
        ax.scatter(model.params.keys()[1:], model.params[1:])
    else:
        ax.errorbar(model.params.keys(), model.params, model.bse*2, fmt='none')    
        ax.scatter(model.params.keys(), model.params)
    return ax

def jitter_binary(a, jitter=0.05):
    """
    Jitter a binary variable.
    """
    return [np.random.rand()*jitter if i==0 else (np.random.rand()*jitter + (1-jitter)) for i in a ]

def bin_edges_equalN(x, bins):
    """
    Creates bin edges to build a histogram where the same number of elements falls in every bin.
    """
    n_points = len(x)
    sortedX = np.sort(x)
    return np.interp(np.linspace(0, n_points, bins+1),
                     np.arange(n_points),
                     sortedX)

def plot_binned_residuals(X, y, model, x_variable=None, bins=40, ax=None):
    """
    Plot of binned residuals for logistic regression model diagnostic.
    """
    if x_variable is None:
        x_variable = model.predict(X)
    residDF = pd.DataFrame()
    residDF['x_variable'] = x_variable
    residDF['resid'] = y - model.predict(X)
    bin_edges = bin_edges_equalN(residDF.x_variable, bins=bins)
    avg_resid = np.zeros(len(bin_edges)-1)
    std_resid = np.zeros(len(bin_edges)-1)
    avg_x = np.zeros(len(bin_edges)-1)
    Ns = np.zeros(len(bin_edges)-1)
    for i in range(len(bin_edges)-1):
        Ns[i] = np.sum((residDF.x_variable >= bin_edges[i]) & (residDF.x_variable < bin_edges[i+1]))
        avg_resid[i] = residDF.loc[(residDF.x_variable >= bin_edges[i]) & (residDF.x_variable < bin_edges[i+1]), 'resid'].mean()
        std_resid[i] = residDF.loc[(residDF.x_variable >= bin_edges[i]) & (residDF.x_variable < bin_edges[i+1]), 'resid'].std()
        avg_x[i] = residDF.loc[(residDF.x_variable >= bin_edges[i]) & (residDF.x_variable < bin_edges[i+1]), 'x_variable'].mean()
    
    if ax is None:
        fig, ax = plt.subplots()
    ax.scatter(avg_x, avg_resid)
    ax.plot(avg_x, +2*std_resid/np.sqrt(Ns), color='grey')
    ax.plot(avg_x, -2*std_resid/np.sqrt(Ns), color='grey')
    return ax
